
#include "xmlparse.h"
#include "DOM.h"

void installCallbacs(XML_Parser parser);

void startElement( void* userData,
                   const char* name,
                   const char** atts );

void endElement( void* userData,
                 const char* name );

void defaultHandler( void* userData,
                     const char* s,
                     int len );

void characterData( void* userData,
                    const char* s,
                    int len );

void PI( void* userData,
         const char* target,
         const char* data );

void unparsedEntityDecl( void* userData,
                         const char* entityName,
                         const char* base,
                         const char* systemId,
                         const char* publicId,
                         const char* notationName );

void notationDecl( void* userData,
                   const char* notationName,
                   const char* base,
                   const char* systemId,
                   const char* publicId );

int externalEntityRef( XML_Parser parser,
                       const char* openEntityNames,
                       const char* base,
                       const char* systemId,
                       const char* publicId );

int unknownEncoding( void* encodingHandlerData,
                     const char* name,
                     XML_Encoding* info );


///////////// implementation ////////////////////////////


void installCallbacs(XML_Parser parser)
{
  XML_SetElementHandler(parser, startElement, endElement);
  XML_SetDefaultHandler(parser, defaultHandler);
  XML_SetCharacterDataHandler(parser, characterData);
  XML_SetProcessingInstructionHandler(parser, PI);
  XML_SetUnparsedEntityDeclHandler(parser, unparsedEntityDecl);
  XML_SetNotationDeclHandler(parser, notationDecl);
  XML_SetExternalEntityRefHandler(parser, externalEntityRef);
}


void  startElement(void* userData,
                   const char* name,
                   const char** atts)
{
  DOMImplementation* domImpl = (DOMImplementation*) userData;
  Document* doc = domImpl->getOwnerDoc();         // get document
  Node* currentNode = domImpl->getCurrentNode();  // get current node

  Element* newElem = doc->createElement((String)name); // new Element node
  currentNode->appendChild(newElem);            // link it in the tree
  domImpl->setCurrentNode(newElem);             // make it the current node

  // add attributes to this new Element
  while (*atts != NULL)
   {
     newElem->setAttribute((String) *atts, (String) *(atts+1));
     atts += 2;
   }

  // this is for debugging expat
  int i;
  for (i = 0; i < domImpl->depth; i++)
     putchar('\t');
  printf("startElement: %s\n", name);
  domImpl->depth += 1;
}


void  endElement(void* userData,
                 const char* name)
{
  DOMImplementation* domImpl = (DOMImplementation*) userData;
  Node* currentNode = domImpl->getCurrentNode(); 
  domImpl->setCurrentNode( currentNode->getParentNode() );

  // this is for debugging expat
  int i;
  domImpl->depth -= 1;
  for (i = 0; i < domImpl->depth; i++)
    putchar('\t');
  printf("endElement: %s\n", name);
}


void defaultHandler(void* userData,
                    const char* s,
                    int len)
{
  // copy s to charData (which is null terminated string)
  XML_Char *cp = (XML_Char*) s;
  char charData[len+1];
  int i;
  for (i=0; i < len; i++)   { charData[i] = *cp;  cp++; }
  charData[len] = 0;

  DOMImplementation* domImpl = (DOMImplementation*) userData;
  Document* doc = domImpl->getOwnerDoc();
  Node* currentNode = domImpl->getCurrentNode(); 

  Text* txtNode = doc->createTextNode(charData);
  currentNode->appendChild(txtNode);
  
  // for debugging expat
  for (i=0; i < domImpl->depth; i++)
    putchar('\t');
  printf("defaultHandler: \n");
  // cout << '\'' << charData << "'\n";
}


void characterData(void* userData,
                   const char* s,
                   int len)
{
  // copy s to charData (which is a null terminated string)
  XML_Char *cp = (XML_Char*) s;
  char charData[len+1];
  int i;
  for (i=0; i < len; i++)   { charData[i] = *cp;  cp++; }
  charData[len] = 0;

  DOMImplementation* domImpl = (DOMImplementation*) userData;
  Document* doc = domImpl->getOwnerDoc();
  Node* currentNode = domImpl->getCurrentNode();

  Text* txtNode = doc->createTextNode(charData);
  currentNode->appendChild(txtNode);
 
  // for debugging expat
  for (i=0; i < domImpl->depth; i++)
    putchar('\t');
  printf("characterData: \n");
  // cout << '\'' << charData << "'\n";
}


void PI(void* userData,
        const char* target,
        const char* data)
{
  int i;
  DOMImplementation* domImpl = (DOMImplementation*) userData;
  for (i = 0; i < domImpl->depth; i++)
   putchar('\t');
  printf("PI: %s %s\n", target, data);
}


void unparsedEntityDecl(void* userData,
                        const char* entityName,
                        const char* base,
                        const char* systemId,
                        const char* publicId,
                        const char* notationName)
{
  int i;
  DOMImplementation* domImpl = (DOMImplementation*) userData;
  for (i = 0; i < domImpl->depth; i++)
   putchar('\t');
  printf("unparsedEntityDecl: %s %s %s %s\n", entityName, systemId, publicId,
                                                  notationName);
}


void notationDecl(void* userData,
                  const char* notationName,
                  const char* base,
                  const char* systemId,
                  const char* publicId)
{
  int i;
  DOMImplementation* domImpl = (DOMImplementation*) userData;
  for (i = 0; i < domImpl->depth; i++)
   putchar('\t');
  printf("notationDecl: %s %s %s\n", notationName, systemId, publicId);
}


int externalEntityRef(XML_Parser parser,
                      const char* openEntityNames,
                      const char* base,
                      const char* systemId,
                      const char* publicId)
{
  printf("externalEntityRef: %s %s %s\n", openEntityNames, systemId, publicId);
  return 0;
}


int unknownEncoding(void* encodingHandlerData,
                    const char* name,
                    XML_Encoding* info)
{
  printf("unknownEncoding: %s\n", name);
  return 0;
}


