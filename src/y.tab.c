
# line 2 "dtd.y"

#define  YYDEBUG  1

#include "DOM.h"

// initialize these global variables with real values
// before calling "yyparse"
Document*       xmlDoc;
DocumentType*   docType;
NamedNodeMap*   elems;
NamedNodeMap*   attribs;

ElementType*    globElemT;
AttrType*       globAttrT;
 

# line 19 "dtd.y"
typedef union 
{
  char*    lexeme;
  Node*    nodePtr;
  int      val;
} YYSTYPE;
# define DTD_ELEMENT 257
# define DTD_EMPTY 258
# define DTD_ANY 259
# define DTD_PCDATA 260
# define DTD_ATTLIST 261
# define DTD_CDATA 262
# define DTD_ID 263
# define DTD_IMPLIED 264
# define DTD_REQUIRED 265
# define DTD_FIXED 266
# define DTD_XMLSPACE 267
# define DTD_DEFAULT 268
# define DTD_PRESERVE 269
# define DTD_COMMA 270
# define DTD_PIPESTEM 271
# define DTD_QUESTION 272
# define DTD_ASTERIX 273
# define DTD_PLUS 274
# define DTD_LPAR 275
# define DTD_RPAR 276
# define DTD_EXCLAM 277
# define DTD_LESS 278
# define DTD_GREATER 279
# define DTD_HYPHEN 280
# define DTD_UNDERSCORE 281
# define DTD_QUOTE 282
# define DTD_COLON 283
# define DTD_NAME 284
# define DTD_NMTOKEN 285
#define yyclearin yychar = -1
#define yyerrok yyerrflag = 0
extern int yychar;
extern int yyerrflag;
#ifndef YYMAXDEPTH
#define YYMAXDEPTH 150
#endif
YYSTYPE yylval, yyval;
typedef int yytabelem;
# define YYERRCODE 256

# line 514 "dtd.y"


#include "src/lex.yy.c"

yyerror(char *s)
{
 fprintf(stderr, "%s", s);
}

int yyparse();

static const yytabelem yyexca[] ={
-1, 1,
	0, -1,
	-2, 0,
	};
# define YYNPROD 57
# define YYLAST 117
static const yytabelem yyact[]={

    29,    71,    70,    72,    47,    34,    79,    77,    60,    50,
    55,    11,    21,    80,    30,    34,    74,    38,    37,    74,
    82,    65,    66,    36,    30,    81,    52,    43,    14,    15,
    40,    51,    42,    39,    68,    23,    24,    25,    59,    54,
    56,    57,     7,    73,    26,    19,     8,    31,    22,     4,
    10,    58,    20,     9,     2,    35,    48,    41,    16,    12,
    33,    28,    32,    27,    18,    17,    13,    75,    67,    64,
    63,    53,    62,    69,    61,     6,     5,    45,    46,    44,
     3,     1,    49,    78,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    76 };
static const yytabelem yypact[]={

 -1000, -1000,  -215,  -215, -1000, -1000, -1000,  -273,  -273, -1000,
  -230, -1000, -1000,  -267, -1000, -1000, -1000, -1000,  -237,  -260,
 -1000, -1000, -1000, -1000, -1000, -1000,  -253,  -243,  -246,  -244,
  -237, -1000,  -270,  -270,  -270,  -275, -1000, -1000, -1000, -1000,
 -1000,  -245,  -234,  -274, -1000,  -231,  -229, -1000, -1000, -1000,
 -1000,  -235,  -276, -1000, -1000, -1000, -1000, -1000,  -241, -1000,
 -1000,  -263, -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000,
 -1000, -1000,  -266, -1000,  -277,  -278, -1000,  -269,  -251, -1000,
 -1000, -1000, -1000 };
static const yytabelem yypgo[]={

     0,    50,    83,    82,    43,    81,    80,    49,    76,    75,
    48,    74,    73,    72,    71,    70,    69,    68,    67,    66,
    47,    65,    64,    44,    63,    62,    61,    60,    57,    56,
    55,    54,    52,    51 };
static const yytabelem yyr1[]={

     0,    31,     5,     6,     6,     7,     7,     8,     1,    19,
    19,    19,    19,    20,    22,    22,    22,    10,    10,    10,
    10,    23,    23,    24,    25,    25,    26,    27,    27,    21,
    21,    14,    14,    28,    28,    32,     9,    30,    30,    33,
    29,     3,    11,    11,    11,    13,    15,    16,    17,     2,
    18,    18,    12,    12,    12,    12,     4 };
static const yytabelem yyr2[]={

     0,     1,     5,     3,     5,     3,     3,     9,     3,     3,
     3,     3,     3,     5,     7,     7,     7,     3,     3,     3,
     1,     5,     3,     5,     5,     7,     5,     5,     7,    11,
     9,     3,     1,     5,     7,     1,    11,     1,     5,     1,
     9,     3,     3,     3,     3,     3,     3,     3,     9,     3,
     1,     7,     3,     3,     5,     3,     7 };
static const yytabelem yychk[]={

 -1000,    -5,   -31,    -6,    -7,    -8,    -9,   257,   261,    -7,
    -1,   284,    -1,   -19,   258,   259,   -20,   -21,   -22,   275,
   -32,   279,   -10,   272,   273,   274,   -23,   -24,   -26,   260,
   284,   -20,   -25,   -27,   275,   -30,   276,   271,   270,   276,
   276,   -28,   276,   271,   -10,   -23,   -23,   279,   -29,    -3,
   284,   276,   271,   -14,   273,   284,   271,   270,   -33,   273,
   284,   -11,   -13,   -15,   -16,   262,   263,   -17,   275,   -12,
   265,   264,   266,    -4,   282,   -18,    -4,   284,    -2,   284,
   282,   276,   271 };
static const yytabelem yydef[]={

     1,    -2,     0,     2,     3,     5,     6,     0,     0,     4,
     0,     8,    35,     0,     9,    10,    11,    12,    20,     0,
    37,     7,    13,    17,    18,    19,     0,     0,     0,     0,
    20,    22,     0,     0,     0,     0,    14,    24,    27,    15,
    16,     0,    32,     0,    21,    23,    26,    36,    38,    39,
    41,     0,     0,    30,    31,    33,    25,    28,     0,    29,
    34,     0,    42,    43,    44,    45,    46,    47,    50,    40,
    52,    53,     0,    55,     0,     0,    54,     0,     0,    49,
    56,    48,    51 };
typedef struct { char *t_name; int t_val; } yytoktype;
#ifndef YYDEBUG
#	define YYDEBUG	0	/* don't allow debugging */
#endif

#if YYDEBUG

yytoktype yytoks[] =
{
	"DTD_ELEMENT",	257,
	"DTD_EMPTY",	258,
	"DTD_ANY",	259,
	"DTD_PCDATA",	260,
	"DTD_ATTLIST",	261,
	"DTD_CDATA",	262,
	"DTD_ID",	263,
	"DTD_IMPLIED",	264,
	"DTD_REQUIRED",	265,
	"DTD_FIXED",	266,
	"DTD_XMLSPACE",	267,
	"DTD_DEFAULT",	268,
	"DTD_PRESERVE",	269,
	"DTD_COMMA",	270,
	"DTD_PIPESTEM",	271,
	"DTD_QUESTION",	272,
	"DTD_ASTERIX",	273,
	"DTD_PLUS",	274,
	"DTD_LPAR",	275,
	"DTD_RPAR",	276,
	"DTD_EXCLAM",	277,
	"DTD_LESS",	278,
	"DTD_GREATER",	279,
	"DTD_HYPHEN",	280,
	"DTD_UNDERSCORE",	281,
	"DTD_QUOTE",	282,
	"DTD_COLON",	283,
	"DTD_NAME",	284,
	"DTD_NMTOKEN",	285,
	"-unknown-",	-1	/* ends search */
};

char * yyreds[] =
{
	"-no such reduction-",
      "dtd : /* empty */",
      "dtd : list_of_declarations",
      "list_of_declarations : decl",
      "list_of_declarations : list_of_declarations decl",
      "decl : elementdecl",
      "decl : AttlistDecl",
      "elementdecl : DTD_ELEMENT el_name contentspec DTD_GREATER",
      "el_name : DTD_NAME",
      "contentspec : DTD_EMPTY",
      "contentspec : DTD_ANY",
      "contentspec : children",
      "contentspec : Mixed",
      "children : choice_seq_list multiplicity_mark",
      "choice_seq_list : DTD_LPAR cp DTD_RPAR",
      "choice_seq_list : DTD_LPAR choice DTD_RPAR",
      "choice_seq_list : DTD_LPAR seq DTD_RPAR",
      "multiplicity_mark : DTD_QUESTION",
      "multiplicity_mark : DTD_ASTERIX",
      "multiplicity_mark : DTD_PLUS",
      "multiplicity_mark : /* empty */",
      "cp : DTD_NAME multiplicity_mark",
      "cp : children",
      "choice : choice_cp_list cp",
      "choice_cp_list : cp DTD_PIPESTEM",
      "choice_cp_list : choice_cp_list cp DTD_PIPESTEM",
      "seq : seq_cp_list cp",
      "seq_cp_list : cp DTD_COMMA",
      "seq_cp_list : seq_cp_list cp DTD_COMMA",
      "Mixed : DTD_LPAR DTD_PCDATA name_list DTD_RPAR DTD_ASTERIX",
      "Mixed : DTD_LPAR DTD_PCDATA DTD_RPAR end_mark",
      "end_mark : DTD_ASTERIX",
      "end_mark : /* empty */",
      "name_list : DTD_PIPESTEM DTD_NAME",
      "name_list : name_list DTD_PIPESTEM DTD_NAME",
      "AttlistDecl : DTD_ATTLIST el_name",
      "AttlistDecl : DTD_ATTLIST el_name AttDef DTD_GREATER",
      "AttDef : /* empty */",
      "AttDef : AttDef OneAttDef",
      "OneAttDef : att_name",
      "OneAttDef : att_name AttType DefaultDecl",
      "att_name : DTD_NAME",
      "AttType : StringType",
      "AttType : TokenizedType",
      "AttType : EnumeratedType",
      "StringType : DTD_CDATA",
      "TokenizedType : DTD_ID",
      "EnumeratedType : Enumeration",
      "Enumeration : DTD_LPAR nmtoken_list list_el DTD_RPAR",
      "list_el : DTD_NAME",
      "nmtoken_list : /* empty */",
      "nmtoken_list : nmtoken_list list_el DTD_PIPESTEM",
      "DefaultDecl : DTD_REQUIRED",
      "DefaultDecl : DTD_IMPLIED",
      "DefaultDecl : DTD_FIXED AttValue",
      "DefaultDecl : AttValue",
      "AttValue : DTD_QUOTE DTD_NAME DTD_QUOTE",
};
#endif /* YYDEBUG */
/*
 * (c) Copyright 1990, OPEN SOFTWARE FOUNDATION, INC.
 * ALL RIGHTS RESERVED
 */
/*
 * OSF/1 Release 1.0
 */
/* @(#)yaccpar	1.3  com/cmd/lang/yacc,3.1, 9/7/89 18:46:37 */
/*
** Skeleton parser driver for yacc output
*/

/*
** yacc user known macros and defines
*/
#ifdef YYSPLIT
#   define YYERROR	return(-2)
#else
#   define YYERROR	goto yyerrlab
#endif

#define YYACCEPT	return(0)
#define YYABORT		return(1)
#define YYBACKUP( newtoken, newvalue )\
{\
	if ( yychar >= 0 || ( yyr2[ yytmp ] >> 1 ) != 1 )\
	{\
		yyerror( "syntax error - cannot backup" );\
		goto yyerrlab;\
	}\
	yychar = newtoken;\
	yystate = *yyps;\
	yylval = newvalue;\
	goto yynewstate;\
}
#define YYRECOVERING()	(!!yyerrflag)
#ifndef YYDEBUG
#	define YYDEBUG	1	/* make debugging available */
#endif

/*
** user known globals
*/
int yydebug;			/* set to 1 to get debugging */

/*
** driver internal defines
*/
#define YYFLAG		(-1000)

#ifdef YYSPLIT
#   define YYSCODE { \
			extern int (*yyf[])(); \
			register int yyret; \
			if (yyf[yytmp]) \
			    if ((yyret=(*yyf[yytmp])()) == -2) \
				    goto yyerrlab; \
				else if (yyret>=0) return(yyret); \
		   }
#endif

/*
** local variables used by the parser
 * these should be static in order to support
 * multiple parsers in a single executable program. POSIX 1003.2-1993
 */
static YYSTYPE yyv[ YYMAXDEPTH ];	/* value stack */
static int yys[ YYMAXDEPTH ];		/* state stack */

static YYSTYPE *yypv;			/* top of value stack */
static YYSTYPE *yypvt;			/* top of value stack for $vars */
static int *yyps;			/* top of state stack */

static int yystate;			/* current state */
static int yytmp;			/* extra var (lasts between blocks) */

/*
** global variables used by the parser - renamed as a result of -p
*/
int yynerrs;			/* number of errors */
int yyerrflag;			/* error recovery flag */
int yychar;			/* current input token number */

/*
** yyparse - return 0 if worked, 1 if syntax error not recovered from
*/
int
yyparse()
{
	/*
	** Initialize externals - yyparse may be called more than once
	*/
	yypv = &yyv[-1];
	yyps = &yys[-1];
	yystate = 0;
	yytmp = 0;
	yynerrs = 0;
	yyerrflag = 0;
	yychar = -1;

	goto yystack;
	{
		register YYSTYPE *yy_pv;	/* top of value stack */
		register int *yy_ps;		/* top of state stack */
		register int yy_state;		/* current state */
		register int  yy_n;		/* internal state number info */

		/*
		** get globals into registers.
		** branch to here only if YYBACKUP was called.
		*/
	yynewstate:
		yy_pv = yypv;
		yy_ps = yyps;
		yy_state = yystate;
		goto yy_newstate;

		/*
		** get globals into registers.
		** either we just started, or we just finished a reduction
		*/
	yystack:
		yy_pv = yypv;
		yy_ps = yyps;
		yy_state = yystate;

		/*
		** top of for (;;) loop while no reductions done
		*/
	yy_stack:
		/*
		** put a state and value onto the stacks
		*/
#if YYDEBUG
		/*
		** if debugging, look up token value in list of value vs.
		** name pairs.  0 and negative (-1) are special values.
		** Note: linear search is used since time is not a real
		** consideration while debugging.
		*/
		if ( yydebug )
		{
			register int yy_i;

			printf( "State %d, token ", yy_state );
			if ( yychar == 0 )
				printf( "end-of-file\n" );
			else if ( yychar < 0 )
				printf( "-none-\n" );
			else
			{
				for ( yy_i = 0; yytoks[yy_i].t_val >= 0;
					yy_i++ )
				{
					if ( yytoks[yy_i].t_val == yychar )
						break;
				}
				printf( "%s\n", yytoks[yy_i].t_name );
			}
		}
#endif /* YYDEBUG */
		if ( ++yy_ps >= &yys[ YYMAXDEPTH ] )	/* room on stack? */
		{
			yyerror( "yacc stack overflow" );
			YYABORT;
		}
		*yy_ps = yy_state;
		*++yy_pv = yyval;

		/*
		** we have a new state - find out what to do
		*/
	yy_newstate:
		if ( ( yy_n = yypact[ yy_state ] ) <= YYFLAG )
			goto yydefault;		/* simple state */
#if YYDEBUG
		/*
		** if debugging, need to mark whether new token grabbed
		*/
		yytmp = yychar < 0;
#endif
		if ( ( yychar < 0 ) && ( ( yychar = yylex() ) < 0 ) )
			yychar = 0;		/* reached EOF */
#if YYDEBUG
		if ( yydebug && yytmp )
		{
			register int yy_i;

			printf( "Received token " );
			if ( yychar == 0 )
				printf( "end-of-file\n" );
			else if ( yychar < 0 )
				printf( "-none-\n" );
			else
			{
				for ( yy_i = 0; yytoks[yy_i].t_val >= 0;
					yy_i++ )
				{
					if ( yytoks[yy_i].t_val == yychar )
						break;
				}
				printf( "%s\n", yytoks[yy_i].t_name );
			}
		}
#endif /* YYDEBUG */
		if ( ( ( yy_n += yychar ) < 0 ) || ( yy_n >= YYLAST ) )
			goto yydefault;
		if ( yychk[ yy_n = yyact[ yy_n ] ] == yychar )	/*valid shift*/
		{
			yychar = -1;
			yyval = yylval;
			yy_state = yy_n;
			if ( yyerrflag > 0 )
				yyerrflag--;
			goto yy_stack;
		}

	yydefault:
		if ( ( yy_n = yydef[ yy_state ] ) == -2 )
		{
#if YYDEBUG
			yytmp = yychar < 0;
#endif
			if ( ( yychar < 0 ) && ( ( yychar = yylex() ) < 0 ) )
				yychar = 0;		/* reached EOF */
#if YYDEBUG
			if ( yydebug && yytmp )
			{
				register int yy_i;

				printf( "Received token " );
				if ( yychar == 0 )
					printf( "end-of-file\n" );
				else if ( yychar < 0 )
					printf( "-none-\n" );
				else
				{
					for ( yy_i = 0;
						yytoks[yy_i].t_val >= 0;
						yy_i++ )
					{
						if ( yytoks[yy_i].t_val
							== yychar )
						{
							break;
						}
					}
					printf( "%s\n", yytoks[yy_i].t_name );
				}
			}
#endif /* YYDEBUG */
			/*
			** look through exception table
			*/
			{
				register const int *yyxi = yyexca;

				while ( ( *yyxi != -1 ) ||
					( yyxi[1] != yy_state ) )
				{
					yyxi += 2;
				}
				while ( ( *(yyxi += 2) >= 0 ) &&
					( *yyxi != yychar ) )
					;
				if ( ( yy_n = yyxi[1] ) < 0 )
					YYACCEPT;
			}
		}

		/*
		** check for syntax error
		*/
		if ( yy_n == 0 )	/* have an error */
		{
			/* no worry about speed here! */
			switch ( yyerrflag )
			{
			case 0:		/* new error */
				yyerror( "syntax error" );
				goto skip_init;
			yyerrlab:
				/*
				** get globals into registers.
				** we have a user generated syntax type error
				*/
				yy_pv = yypv;
				yy_ps = yyps;
				yy_state = yystate;
				yynerrs++;
			skip_init:
			case 1:
			case 2:		/* incompletely recovered error */
					/* try again... */
				yyerrflag = 3;
				/*
				** find state where "error" is a legal
				** shift action
				*/
				while ( yy_ps >= yys )
				{
					yy_n = yypact[ *yy_ps ] + YYERRCODE;
					if ( yy_n >= 0 && yy_n < YYLAST &&
						yychk[yyact[yy_n]] == YYERRCODE)					{
						/*
						** simulate shift of "error"
						*/
						yy_state = yyact[ yy_n ];
						goto yy_stack;
					}
					/*
					** current state has no shift on
					** "error", pop stack
					*/
#if YYDEBUG
#	define _POP_ "Error recovery pops state %d, uncovers state %d\n"
					if ( yydebug )
						printf( _POP_, *yy_ps,
							yy_ps[-1] );
#	undef _POP_
#endif
					yy_ps--;
					yy_pv--;
				}
				/*
				** there is no state on stack with "error" as
				** a valid shift.  give up.
				*/
				YYABORT;
			case 3:		/* no shift yet; eat a token */
#if YYDEBUG
				/*
				** if debugging, look up token in list of
				** pairs.  0 and negative shouldn't occur,
				** but since timing doesn't matter when
				** debugging, it doesn't hurt to leave the
				** tests here.
				*/
				if ( yydebug )
				{
					register int yy_i;

					printf( "Error recovery discards " );
					if ( yychar == 0 )
						printf( "token end-of-file\n" );
					else if ( yychar < 0 )
						printf( "token -none-\n" );
					else
					{
						for ( yy_i = 0;
							yytoks[yy_i].t_val >= 0;
							yy_i++ )
						{
							if ( yytoks[yy_i].t_val
								== yychar )
							{
								break;
							}
						}
						printf( "token %s\n",
							yytoks[yy_i].t_name );
					}
				}
#endif /* YYDEBUG */
				if ( yychar == 0 )	/* reached EOF. quit */
					YYABORT;
				yychar = -1;
				goto yy_newstate;
			}
		}/* end if ( yy_n == 0 ) */
		/*
		** reduction by production yy_n
		** put stack tops, etc. so things right after switch
		*/
#if YYDEBUG
		/*
		** if debugging, print the string that is the user's
		** specification of the reduction which is just about
		** to be done.
		*/
		if ( yydebug )
			printf( "Reduce by (%d) \"%s\"\n",
				yy_n, yyreds[ yy_n ] );
#endif
		yytmp = yy_n;			/* value to switch over */
		yypvt = yy_pv;			/* $vars top of value stack */
		/*
		** Look in goto table for next state
		** Sorry about using yy_state here as temporary
		** register variable, but why not, if it works...
		** If yyr2[ yy_n ] doesn't have the low order bit
		** set, then there is no action to be done for
		** this reduction.  So, no saving & unsaving of
		** registers done.  The only difference between the
		** code just after the if and the body of the if is
		** the goto yy_stack in the body.  This way the test
		** can be made before the choice of what to do is needed.
		*/
		{
			/* length of production doubled with extra bit */
			register int yy_len = yyr2[ yy_n ];

			if ( !( yy_len & 01 ) )
			{
				yy_len >>= 1;
				yyval = ( yy_pv -= yy_len )[1];	/* $$ = $1 */
				yy_state = yypgo[ yy_n = yyr1[ yy_n ] ] +
					*( yy_ps -= yy_len ) + 1;
				if ( yy_state >= YYLAST ||
					yychk[ yy_state =
					yyact[ yy_state ] ] != -yy_n )
				{
					yy_state = yyact[ yypgo[ yy_n ] ];
				}
				goto yy_stack;
			}
			yy_len >>= 1;
			yyval = ( yy_pv -= yy_len )[1];	/* $$ = $1 */
			yy_state = yypgo[ yy_n = yyr1[ yy_n ] ] +
				*( yy_ps -= yy_len ) + 1;
			if ( yy_state >= YYLAST ||
				yychk[ yy_state = yyact[ yy_state ] ] != -yy_n )
			{
				yy_state = yyact[ yypgo[ yy_n ] ];
			}
		}
					/* save until reenter driver code */
		yystate = yy_state;
		yyps = yy_ps;
		yypv = yy_pv;
	}
	/*
	** code supplied by user is placed in this switch
	*/

		switch(yytmp){

case 1:
# line 58 "dtd.y"
{
				  yydebug = 0;
				} /*NOTREACHED*/ break;
case 2:
# line 62 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 3:
# line 68 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 4:
# line 72 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 5:
# line 78 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 6:
# line 82 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 7:
# line 93 "dtd.y"
{
				  // first look if this element exist in list
				  ElementType* elemT = (ElementType*) elems->getNamedItem(yypvt[-2].lexeme);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType(yypvt[-2].lexeme);
				  elems->setNamedItem(elemT);

				  // link ContentType to this ElementType
				  elemT->setContentType((ContentType*) yypvt[-1].nodePtr);
				} /*NOTREACHED*/ break;
case 8:
# line 108 "dtd.y"
{
				  yyval.lexeme = yypvt[-0].lexeme;
				} /*NOTREACHED*/ break;
case 9:
# line 115 "dtd.y"
{
				  ContentType* contT = xmlDoc->createContentType();
				  contT->setCType(ContentType::EMPTY);
				  yyval.nodePtr = contT;
				} /*NOTREACHED*/ break;
case 10:
# line 122 "dtd.y"
{
				  ContentType* contT = xmlDoc->createContentType();
				  contT->setCType(ContentType::ANY);
				  yyval.nodePtr = contT;
				} /*NOTREACHED*/ break;
case 11:
# line 129 "dtd.y"
{
				  ((ContentType*) yypvt[-0].nodePtr)->setCType(ContentType::CHILDREN);
				  yyval.nodePtr = yypvt[-0].nodePtr;
				} /*NOTREACHED*/ break;
case 12:
# line 135 "dtd.y"
{
				  ((ContentType*) yypvt[-0].nodePtr)->setCType(ContentType::MIXED);
				  yyval.nodePtr = yypvt[-0].nodePtr;
				} /*NOTREACHED*/ break;
case 13:
# line 143 "dtd.y"
{
				  ((ContentType*) yypvt[-1].nodePtr)->setMultiplicity(yypvt[-0].val);
				  yyval.nodePtr = yypvt[-1].nodePtr;
				} /*NOTREACHED*/ break;
case 14:
# line 151 "dtd.y"
{
				  ContentType* contT = xmlDoc->createContentType();
				  try { contT->appendChild(yypvt[-1].nodePtr); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  yyval.nodePtr = contT;
				} /*NOTREACHED*/ break;
case 15:
# line 160 "dtd.y"
{
				  ((ContentType*) yypvt[-1].nodePtr)->setOrdering(ContentType::CHOICE);
				  yyval.nodePtr = yypvt[-1].nodePtr;
				} /*NOTREACHED*/ break;
case 16:
# line 166 "dtd.y"
{
				  ((ContentType*) yypvt[-1].nodePtr)->setOrdering(ContentType::SEQUENCE);
				  yyval.nodePtr = yypvt[-1].nodePtr;
				} /*NOTREACHED*/ break;
case 17:
# line 174 "dtd.y"
{
				  yyval.val = ContentType::ZERO_OR_ONE;
				} /*NOTREACHED*/ break;
case 18:
# line 179 "dtd.y"
{
				  yyval.val = ContentType::ZERO_OR_MORE;
				} /*NOTREACHED*/ break;
case 19:
# line 184 "dtd.y"
{
				  yyval.val = ContentType::ONE_OR_MORE;
				} /*NOTREACHED*/ break;
case 20:
# line 189 "dtd.y"
{
				  yyval.val = ContentType::ONLY_ONE;
				} /*NOTREACHED*/ break;
case 21:
# line 197 "dtd.y"
{
				  // first look if this element exist in list
				  ElementType* elemT = (ElementType*) elems->getNamedItem(yypvt[-1].lexeme);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType(yypvt[-1].lexeme);
				  elems->setNamedItem(elemT);

				  elemT->setMultiplicity(yypvt[-0].val);
				  yyval.nodePtr = elemT;
				} /*NOTREACHED*/ break;
case 22:
# line 210 "dtd.y"
{
				  yyval.nodePtr = yypvt[-0].nodePtr;
				} /*NOTREACHED*/ break;
case 23:
# line 217 "dtd.y"
{
				  try { yypvt[-1].nodePtr->appendChild(yypvt[-0].nodePtr); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  yyval.nodePtr = yypvt[-1].nodePtr;
				} /*NOTREACHED*/ break;
case 24:
# line 227 "dtd.y"
{
				  ContentType* contT = xmlDoc->createContentType();
				  try { contT->appendChild(yypvt[-1].nodePtr); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  yyval.nodePtr = contT;
				} /*NOTREACHED*/ break;
case 25:
# line 236 "dtd.y"
{
				  try { yypvt[-2].nodePtr->appendChild(yypvt[-1].nodePtr); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  yyval.nodePtr = yypvt[-2].nodePtr;
				} /*NOTREACHED*/ break;
case 26:
# line 246 "dtd.y"
{
				  try { yypvt[-1].nodePtr->appendChild(yypvt[-0].nodePtr); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  yyval.nodePtr = yypvt[-1].nodePtr;
				} /*NOTREACHED*/ break;
case 27:
# line 256 "dtd.y"
{
				  ContentType* contT = xmlDoc->createContentType();
				  try { contT->appendChild(yypvt[-1].nodePtr); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  yyval.nodePtr = contT;
				} /*NOTREACHED*/ break;
case 28:
# line 265 "dtd.y"
{
				  try { yypvt[-2].nodePtr->appendChild(yypvt[-1].nodePtr); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  yyval.nodePtr = yypvt[-2].nodePtr;
				} /*NOTREACHED*/ break;
case 29:
# line 276 "dtd.y"
{
				  // set the multiplicity
				  ((ContentType*) yypvt[-2].nodePtr)->setMultiplicity(ContentType::ZERO_OR_MORE);
				  ((ContentType*) yypvt[-2].nodePtr)->setOrdering(ContentType::CHOICE);

				  // insert PCDATA in front of the list of children
				  PCDATA* pcd = xmlDoc->createPCDATA();
				  Node* first = yypvt[-2].nodePtr->getFirstChild();
				  try { yypvt[-2].nodePtr->insertBefore(pcd, first); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }

				  yyval.nodePtr = yypvt[-2].nodePtr;
				} /*NOTREACHED*/ break;
case 30:
# line 292 "dtd.y"
{
				  ContentType* contT = xmlDoc->createContentType();
				  PCDATA* pcd = xmlDoc->createPCDATA();
				  contT->appendChild(pcd);
				  contT->setMultiplicity(yypvt[-0].val);
				  yyval.nodePtr = contT;
				} /*NOTREACHED*/ break;
case 31:
# line 303 "dtd.y"
{
				  yyval.val = ContentType::ZERO_OR_MORE;
				} /*NOTREACHED*/ break;
case 32:
# line 308 "dtd.y"
{
				  yyval.val = ContentType::ONLY_ONE;
				} /*NOTREACHED*/ break;
case 33:
# line 315 "dtd.y"
{
				  // first look if NAME exist in the list of elements
				  ElementType* elemT = (ElementType*) elems->getNamedItem(yypvt[-0].lexeme);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType(yypvt[-0].lexeme);
				  elems->setNamedItem(elemT);

				  // now create a ContentType and append the element to it
				  ContentType* contT = xmlDoc->createContentType();
				  try { contT->appendChild(elemT); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }

				  yyval.nodePtr = contT;
				} /*NOTREACHED*/ break;
case 34:
# line 333 "dtd.y"
{
				  // first look if NAME exist in the list of elements
				  ElementType* elemT = (ElementType*) elems->getNamedItem(yypvt[-0].lexeme);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType(yypvt[-0].lexeme);
				  elems->setNamedItem(elemT);

				  // now append the element to ContentType returned by name_list
				  try { yypvt[-2].nodePtr->appendChild(elemT); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }

				  yyval.nodePtr = yypvt[-2].nodePtr;
				} /*NOTREACHED*/ break;
case 35:
# line 356 "dtd.y"
{
				  // first look if el_name exist in the list of elements
				  ElementType* elemT = (ElementType*) elems->getNamedItem(yypvt[-0].lexeme);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType(yypvt[-0].lexeme);
				  elems->setNamedItem(elemT);

				  globElemT = elemT;
				} /*NOTREACHED*/ break;
case 36:
# line 367 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 37:
# line 373 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 38:
# line 377 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 39:
# line 383 "dtd.y"
{
				  // first look if att_name exist in the list of attributes
				  AttrType* attrT = (AttrType*) attribs->getNamedItem(yypvt[-0].lexeme);

				  // if not, create and insert it
				  if (attrT == NULL)  attrT = xmlDoc->createAttrType(yypvt[-0].lexeme);
				  attribs->setNamedItem(attrT);

				  // add it to the list of element
				  try { globElemT->setAttributeNode(attrT); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }

				  attrT->setParentNode(globElemT);

				  globAttrT = attrT;
				} /*NOTREACHED*/ break;
case 40:
# line 401 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 41:
# line 407 "dtd.y"
{
				  yyval.lexeme = yypvt[-0].lexeme;
				} /*NOTREACHED*/ break;
case 42:
# line 414 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 43:
# line 418 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 44:
# line 422 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 45:
# line 428 "dtd.y"
{
				  globAttrT->setType(AttrType::CDATA);
				} /*NOTREACHED*/ break;
case 46:
# line 435 "dtd.y"
{
				  globAttrT->setType(AttrType::ID);
				} /*NOTREACHED*/ break;
case 47:
# line 449 "dtd.y"
{
				  globAttrT->setType(AttrType::ENUMERATION);
				} /*NOTREACHED*/ break;
case 48:
# line 458 "dtd.y"
{
				  globAttrT->insertEnumValue(yypvt[-1].lexeme);
				} /*NOTREACHED*/ break;
case 49:
# line 465 "dtd.y"
{
				  yyval.lexeme = yypvt[-0].lexeme;
				} /*NOTREACHED*/ break;
case 50:
# line 474 "dtd.y"
{
				} /*NOTREACHED*/ break;
case 51:
# line 478 "dtd.y"
{
				  globAttrT->insertEnumValue(yypvt[-1].lexeme);
				} /*NOTREACHED*/ break;
case 52:
# line 485 "dtd.y"
{
				  globAttrT->setDefaultType(AttrType::REQUIRED);
				} /*NOTREACHED*/ break;
case 53:
# line 490 "dtd.y"
{
				  globAttrT->setDefaultType(AttrType::IMPLIED);
				} /*NOTREACHED*/ break;
case 54:
# line 495 "dtd.y"
{
				  globAttrT->setDefaultType(AttrType::FIXED);
				  globAttrT->setValue(yypvt[-0].lexeme);
				} /*NOTREACHED*/ break;
case 55:
# line 501 "dtd.y"
{
				  globAttrT->setDefaultType(AttrType::DEFAULT);
				  globAttrT->setValue(yypvt[-0].lexeme);
				} /*NOTREACHED*/ break;
case 56:
# line 509 "dtd.y"
{
				  yyval.lexeme = yypvt[-1].lexeme;
				} /*NOTREACHED*/ break;
}


	goto yystack;		/* reset registers in driver code */
}
