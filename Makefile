
CC=g++
CFLAGS= -g -Iinclude
LIB= lib/DOM.a lib/expat.a
OBJS=lib/DOMException.o \
     lib/Node.o \
     lib/Document.o \
     lib/NodeList.o \
     lib/NamedNodeMap.o \
     lib/CharacterData.o \
     lib/Attr.o \
     lib/Element.o \
     lib/Text.o \
     lib/Notation.o \
     lib/ElementType.o \
     lib/AttrType.o \
     lib/ContentType.o \
     lib/DocumentType.o \
     lib/DOMImplementation.o


all: exc node doc nodelist nnm crd attr \
     elem txt not et at ctt dt impl    arch    clean


arch: $(OBJS)
	ar cr lib/DOM.a $(OBJS)

clean:
	rm $(OBJS)

tst: src/tst.C $(LIB)
	$(CC) $(CFLAGS) -o src/tst src/tst.C $(LIB)

exc: src/DOMException.C include/DOMException.h
	$(CC) $(CFLAGS) -c -o lib/DOMException.o src/DOMException.C

node: src/Node.C include/Node.h
	$(CC) $(CFLAGS) -c -o lib/Node.o src/Node.C

doc: src/Document.C include/Document.h
	$(CC) $(CFLAGS) -c -o lib/Document.o src/Document.C

nodelist: src/NodeList.C  include/NodeList.h
	$(CC) $(CFLAGS) -c -o lib/NodeList.o src/NodeList.C

nnm: src/NamedNodeMap.C  include/NamedNodeMap.h
	$(CC) $(CFLAGS) -c -o lib/NamedNodeMap.o src/NamedNodeMap.C

crd: src/CharacterData.C  include/CharacterData.h
	$(CC) $(CFLAGS) -c -o lib/CharacterData.o src/CharacterData.C

attr:  src/Attr.C  include/Attr.h
	$(CC) $(CFLAGS) -c -o lib/Attr.o src/Attr.C

elem:  src/Element.C  include/Element.h
	$(CC) $(CFLAGS) -c -o lib/Element.o src/Element.C

txt:  src/Text.C  include/Text.h
	$(CC) $(CFLAGS) -c -o lib/Text.o src/Text.C

not:  src/Notation.C  include/Notation.h
	$(CC) $(CFLAGS) -c -o lib/Notation.o src/Notation.C

et:  src/ElementType.C  include/ElementType.h
	$(CC) $(CFLAGS) -c -o lib/ElementType.o src/ElementType.C

at:  src/AttrType.C  include/AttrType.h
	$(CC) $(CFLAGS) -c -o lib/AttrType.o src/AttrType.C

ctt:  src/ContentType.C  include/ContentType.h
	$(CC) $(CFLAGS) -c -o lib/ContentType.o src/ContentType.C

dt:  src/DocumentType.C  include/DocumentType.h
	$(CC) $(CFLAGS) -c -o lib/DocumentType.o src/DocumentType.C

impl:  src/DOMImplementation.C  include/DOMImplementation.h
	$(CC) $(CFLAGS) -c -o lib/DOMImplementation.o src/DOMImplementation.C


