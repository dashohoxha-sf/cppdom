
typedef union 
{
  char*    lexeme;
  Node*    nodePtr;
  int      val;
} YYSTYPE;
extern YYSTYPE yylval;
# define DTD_ELEMENT 257
# define DTD_EMPTY 258
# define DTD_ANY 259
# define DTD_PCDATA 260
# define DTD_ATTLIST 261
# define DTD_CDATA 262
# define DTD_ID 263
# define DTD_IMPLIED 264
# define DTD_REQUIRED 265
# define DTD_FIXED 266
# define DTD_XMLSPACE 267
# define DTD_DEFAULT 268
# define DTD_PRESERVE 269
# define DTD_COMMA 270
# define DTD_PIPESTEM 271
# define DTD_QUESTION 272
# define DTD_ASTERIX 273
# define DTD_PLUS 274
# define DTD_LPAR 275
# define DTD_RPAR 276
# define DTD_EXCLAM 277
# define DTD_LESS 278
# define DTD_GREATER 279
# define DTD_HYPHEN 280
# define DTD_UNDERSCORE 281
# define DTD_QUOTE 282
# define DTD_COLON 283
# define DTD_NAME 284
# define DTD_NMTOKEN 285
