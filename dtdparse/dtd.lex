%{
/*

C++DOM, Implementation of DOM Core in C++.
Copyright (C) 1999 Dashamir Hoxha & Aurel Cami

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


E-mail:
   Aurel    Cami  : e103686@ceng.metu.edu.tr
   Dashamir Hoxha : e106708@ceng.metu.edu.tr

H-page:
   Aurel    Cami  : http://www.ceng.metu.edu.tr/~e103686/home_page
   Dashamir Hoxha : http://www.ceng.metu.edu.tr/~e106708

*/  

int new_line_cnt = 1;

%}
comma          ","
pipestem       "|"
question       "?"
asterix        "*"
plus           "+"
lpar           "("
rpar           ")"
exclam         "!"
less           "<"
greater        ">"
hyphen         "-"
underscore     "_"
period         "."
colon          ":"
quote          "\""
letter         [a-zA-Z]
digit          [0-9]
namechar       {letter}|{digit}|{period}|{hyphen}|{underscore}|{colon} 
name           ({letter}|{underscore}|{colon})({namechar})*
nmtoken        ({namechar})*
white          [ \t]
start_com      "<!--"
end_com        "-->"
%%
"<!ELEMENT"      {
		  /* 
		   printf ("DTD_ELEMENT ");
                   printf ("%s \n", yytext);	
		  */
		   return DTD_ELEMENT ;
                 }
#PCDATA          {
		  /* 
		   printf ("DTD_PCDATA");
                   printf ("%s \n", yytext);	
		  */
		   return DTD_PCDATA ;
                 }
EMPTY            {
		  /* 
		   printf ("DTD_EMPTY");
                   printf ("%s \n", yytext);	
		  */
     	           return DTD_EMPTY ;
                 }
ANY              {
		  /* 
		   printf ("DTD_ANY");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_ANY ;
		 }
"<!ATTLIST"      {
		  /* 
		   printf ("DTD_ATTLIST");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_ATTLIST ;
                 }
CDATA            {
		  /* 
		   printf ("DTD_CDATA");
                   printf ("%s \n", yytext);	
		  */
		   return DTD_CDATA ;
                 }
ID               {
		  /* 
		   printf ("DTD_ID");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_ID ;
                 }
#REQUIRED        { 
		  /* 
		   printf ("DTD_REQUIRED");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_REQUIRED ;
		 }
#IMPLIED         {
		  /* 
		   printf ("DTD_IMPLIED");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_IMPLIED ;
                 }
#FIXED           {
		  /* 
		   printf ("DTD_FIXED");
                   printf ("%s \n", yytext);	
		  */
	           return DTD_FIXED ;		
                 }
xml-space        {
		  /* 
		   printf ("DTD_XMLSPACE");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_XMLSPACE ;
                 }
default          {
		  /* 
		   printf ("DTD_DEFAULT");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_DEFAULT ;
                 }
preserve         {
		  /* 
		   printf ("DTD_PRESERVE");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_PRESERVE ;
                 } 
{quote}          {
		  /* 
		   printf ("DTD_QUOTE");
                   printf ("%s \n", yytext);	
		  */
                    return DTD_QUOTE ;
                 } 
{less}           {
		  /* 
		   printf ("DTD_LESS");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_LESS ;
                 } 
{greater}        {
		  /* 
		   printf ("DTD_GREATER");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_GREATER ;
                 } 
{exclam}         {
		  /* 
		   printf ("DTD_EXCLAM");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_EXCLAM ;
                 }
{lpar}           {
		  /* 
		   printf ("DTD_LPAR");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_LPAR ;
                 }
{rpar}           {
		  /* 
		   printf ("DTD_RPAR");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_RPAR ;
                 }  
{comma}          {
		  /* 
		   printf ("DTD_COMMA");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_COMMA ;
                 } 
{pipestem}       {
		  /* 
		   printf ("DTD_PIPESTEM");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_PIPESTEM ;
                 }  
{asterix}        {
		  /* 
		   printf ("DTD_ASTERIX");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_ASTERIX ;
		 }  
{plus}           {
		  /* 
		   printf ("DTD_PLUS");
                   printf ("%s \n", yytext);	
		  */
 		   return DTD_PLUS ;
                 } 
{question}       {
		  /* 
		   printf ("DTD_QUESTION");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_QUESTION ;
                 } 
{name}           { 
                   if( yyleng >= 3)
                      if (   (yytext[0] == 'x' || yytext[0] == 'X') 
                          && (yytext[1] == 'm' || yytext[1] == 'M') 
                          && (yytext[2] == 'l' || yytext[2] == 'L')  )
                          fprintf(stderr, "name that starts with xml at line %d \n\n",
                                           new_line_cnt); 
		  /* 
		   printf ("DTD_NAME");
		   printf("%s\n", yytext);
		  */
		   
		   yylval.lexeme = new char[yyleng + 1];
		   strcpy(yylval.lexeme, yytext );

                   return DTD_NAME ;
                 } 
{nmtoken}        { 
                   if( yyleng >= 3)
                      if (   (yytext[0] == 'x' || yytext[0] == 'X') 
                          && (yytext[1] == 'm' || yytext[1] == 'M') 
                          && (yytext[2] == 'l' || yytext[2] == 'L')  )
                          fprintf(stderr, "nmtoken that starts with xml at line %d \n\n",
                                           new_line_cnt); 
		  /* 
		   printf ("DTD_NMTOKEN");
                   printf ("%s \n", yytext);	
		  */
                   return DTD_NMTOKEN ;
                 }
{start_com}      {
                    char ch;
                    int i = new_line_cnt;   

		    while(1)
                    {
                       if ( '-' == (ch = yyinput()) )
                       {
                          if ( '-' == (ch = yyinput()) )
                          {
                             if ( '>' == (ch = yyinput()) )   break;
                             else if ( ch == '\n' )
                             {
			        new_line_cnt++;
                             }	
			     else if ( ch == '\0' )
                             {
                                 fprintf(stderr, "\nunterminated comment started at line: %d\n\n", i);
                                 return -1;
                             }
                          }
                          else if ( ch == '\n' )
                          {
			     new_line_cnt++;
                          }	
			  else if ( ch == '\0' )
		          {
                             fprintf(stderr, "\nunterminated comment started at line: %d\n\n", i);
                             return -1;
			  } 
                       }  
                       else if ( ch == '\n' )
                       {
		         new_line_cnt++;
                       }	
		       else if ( ch == '\0' )
                       {
                           fprintf(stderr, "\nunterminated comment started at line: %d\n\n", i);	
                           return -1;
                       } 
                    }
                 }
{white}          ;
"\n"             {
                   new_line_cnt++;
                 }
.                {
                    if ( yytext[0] == '\0' )
                    {
                         printf("\n\n");
                         return -1; 
                    }
                    fprintf(stderr, "line %d illegal symbol\n", new_line_cnt);
		 }
