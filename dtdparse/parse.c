
#include <stdio.h>
#include "y.tab.c"

main(int argc, char* argv[])
{
  if (argc == 1)
    {
      fprintf(stderr, "usage: %s <dtdFile> \n", argv[0]);
      exit(1);
    }

  // redirect stdin to fileName
  if ( freopen(argv[1], "r", stdin) == NULL )
    {
      fprintf(stderr, "cannot open file: %s for reading", argv[1]);
      exit(1);
    }

  // initialize global variables used by dtd parser
  xmlDoc  = new Document("xml");  // the owner of this DOMImplementation instance
  docType = xmlDoc->getDoctype();
  elems   = docType->getElements();
  attribs = docType->getAttributes();

  // now parse by calling "yyparse"
  // this builds the dtd structure
  try { yyparse(); }
  catch (DOMException& e)
      { cout << e << endl;  exit(1); }

  return 0;
}
