%{

/*

C++DOM, Implementation of DOM Core in C++.
Copyright (C) 1999 Dashamir Hoxha & Aurel Cami

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


E-mail:
   Aurel    Cami  : e103686@ceng.metu.edu.tr
   Dashamir Hoxha : e106708@ceng.metu.edu.tr

H-page:
   Aurel    Cami  : http://www.ceng.metu.edu.tr/~e103686/home_page
   Dashamir Hoxha : http://www.ceng.metu.edu.tr/~e106708

*/
  

#define  YYDEBUG  1

#include "DOM.h"

// initialize these global variables with real values
// before calling "yyparse"
Document*       xmlDoc;
DocumentType*   docType;
NamedNodeMap*   elems;
NamedNodeMap*   attribs;

ElementType*    globElemT;
AttrType*       globAttrT;
 
%}

%union
{
  char*    lexeme;
  Node*    nodePtr;
  int      val;
}

/* element keywords */

%token  DTD_ELEMENT  DTD_EMPTY  DTD_ANY  DTD_PCDATA  

/* attribute keywords */

%token  DTD_ATTLIST  DTD_CDATA  DTD_ID  DTD_IMPLIED  DTD_REQUIRED
        DTD_FIXED  DTD_XMLSPACE  DTD_DEFAULT  DTD_PRESERVE

/* special characters */

%token  DTD_COMMA  DTD_PIPESTEM  DTD_QUESTION  DTD_ASTERIX  DTD_PLUS
        DTD_LPAR  DTD_RPAR  DTD_EXCLAM  DTD_LESS  DTD_GREATER  DTD_HYPHEN
        DTD_UNDERSCORE  DTD_QUOTE  DTD_COLON

/* names */

%token  <lexeme>  DTD_NAME  DTD_NMTOKEN

%type   <lexeme>  el_name  list_el  att_name  AttValue

%type  <val>  dtd  list_of_declarations  decl  elementdecl  AttlistDecl
%type  <val>  multiplicity_mark  AttType  DefaultDecl  StringType  end_mark
%type  <val>  TokenizedType  EnumeratedType  Enumeration  nmtoken_list

%type  <nodePtr>  contentspec  children  Mixed  choice_seq_list  cp  choice
%type  <nodePtr>  choice_cp_list  seq  seq_cp_list  Mixed  name_list
%type  <nodePtr>  OneAttDef  AttDef

%start dtd
 
%%
dtd			:      	{
				  yydebug = 0;
				}	
				list_of_declarations
				{
				}
			;


list_of_declarations	:	decl
				{
				}

			|	list_of_declarations  decl
				{
				}
			;


decl			:	elementdecl
				{
				}

			|	AttlistDecl
				{
				}

	       /*	|	EntityDecl     */
			;  	


/* 1) Element Declarations */


elementdecl		:	DTD_ELEMENT  el_name  contentspec DTD_GREATER
				{
				  // first look if this element exist in list
				  ElementType* elemT = (ElementType*) elems->getNamedItem($2);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType($2);
				  elems->setNamedItem(elemT);

				  // link ContentType to this ElementType
				  elemT->setContentType((ContentType*) $3);
				}
			; 


el_name			:	DTD_NAME
				{
				  $$ = $1;
				}
			;


contentspec		:	DTD_EMPTY
				{
				  ContentType* contT = xmlDoc->createContentType();
				  contT->setCType(ContentType::EMPTY);
				  $$ = contT;
				}

			|	DTD_ANY
				{
				  ContentType* contT = xmlDoc->createContentType();
				  contT->setCType(ContentType::ANY);
				  $$ = contT;
				}

			|	children
				{
				  ((ContentType*) $1)->setCType(ContentType::CHILDREN);
				  $$ = $1;
				}

			|	Mixed
				{
				  ((ContentType*) $1)->setCType(ContentType::MIXED);
				  $$ = $1;
				}
			;


children		:	choice_seq_list   multiplicity_mark
				{
				  ((ContentType*) $1)->setMultiplicity($2);
				  $$ = $1;
				}
			;


choice_seq_list         :       DTD_LPAR  cp  DTD_RPAR
				{
				  ContentType* contT = xmlDoc->createContentType();
				  try { contT->appendChild($2); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  $$ = contT;
				}

			|       DTD_LPAR  choice  DTD_RPAR
				{
				  ((ContentType*) $2)->setOrdering(ContentType::CHOICE);
				  $$ = $2;
				}

			| 	DTD_LPAR  seq  DTD_RPAR
				{
				  ((ContentType*) $2)->setOrdering(ContentType::SEQUENCE);
				  $$ = $2;
				}
			;


multiplicity_mark	:	DTD_QUESTION
				{
				  $$ = ContentType::ZERO_OR_ONE;
				}

			|	DTD_ASTERIX
				{
				  $$ = ContentType::ZERO_OR_MORE;
				}

			|	DTD_PLUS
				{
				  $$ = ContentType::ONE_OR_MORE;
				}

			|	/* empty */
				{
				  $$ = ContentType::ONLY_ONE;
				}
			;


/* content particle */
cp			:	DTD_NAME multiplicity_mark
				{
				  // first look if this element exist in list
				  ElementType* elemT = (ElementType*) elems->getNamedItem($1);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType($1);
				  elems->setNamedItem(elemT);

				  elemT->setMultiplicity($2);
				  $$ = elemT;
				}

			|       children
				{
				  $$ = $1;
				}
			;


choice			:	choice_cp_list  cp
				{
				  try { $1->appendChild($2); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  $$ = $1;
				}
			;


choice_cp_list		:       cp  DTD_PIPESTEM
				{
				  ContentType* contT = xmlDoc->createContentType();
				  try { contT->appendChild($1); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  $$ = contT;
				}

			|	choice_cp_list  cp  DTD_PIPESTEM
				{
				  try { $1->appendChild($2); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  $$ = $1;
				}
			;


seq			:	seq_cp_list  cp
				{
				  try { $1->appendChild($2); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  $$ = $1;
				}
			;


seq_cp_list		:       cp  DTD_COMMA
				{
				  ContentType* contT = xmlDoc->createContentType();
				  try { contT->appendChild($1); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  $$ = contT;
				}

			|	seq_cp_list  cp  DTD_COMMA
				{
				  try { $1->appendChild($2); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }
				  $$ = $1;
				}
			; 
	


Mixed                   :	DTD_LPAR  DTD_PCDATA  name_list  DTD_RPAR DTD_ASTERIX
				{
				  // set the multiplicity
				  ((ContentType*) $3)->setMultiplicity(ContentType::ZERO_OR_MORE);
				  ((ContentType*) $3)->setOrdering(ContentType::CHOICE);

				  // insert PCDATA in front of the list of children
				  PCDATA* pcd = xmlDoc->createPCDATA();
				  Node* first = $3->getFirstChild();
				  try { $3->insertBefore(pcd, first); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }

				  $$ = $3;
				}

			|	DTD_LPAR  DTD_PCDATA  DTD_RPAR  end_mark
				{
				  ContentType* contT = xmlDoc->createContentType();
				  PCDATA* pcd = xmlDoc->createPCDATA();
				  contT->appendChild(pcd);
				  contT->setMultiplicity($4);
				  $$ = contT;
				}
			;


end_mark                :       DTD_ASTERIX
				{
				  $$ = ContentType::ZERO_OR_MORE;
				}

			|	/* empty */
				{
				  $$ = ContentType::ONLY_ONE;
				}
			;


name_list		:       DTD_PIPESTEM  DTD_NAME	
				{
				  // first look if NAME exist in the list of elements
				  ElementType* elemT = (ElementType*) elems->getNamedItem($2);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType($2);
				  elems->setNamedItem(elemT);

				  // now create a ContentType and append the element to it
				  ContentType* contT = xmlDoc->createContentType();
				  try { contT->appendChild(elemT); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }

				  $$ = contT;
				}

			|	name_list  DTD_PIPESTEM  DTD_NAME
				{
				  // first look if NAME exist in the list of elements
				  ElementType* elemT = (ElementType*) elems->getNamedItem($3);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType($3);
				  elems->setNamedItem(elemT);

				  // now append the element to ContentType returned by name_list
				  try { $1->appendChild(elemT); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }

				  $$ = $1;
				}
			;

 

/* 2) Attribute list Declarations */


AttlistDecl		:	DTD_ATTLIST  el_name
				{
				  // first look if el_name exist in the list of elements
				  ElementType* elemT = (ElementType*) elems->getNamedItem($2);

				  // if not, create and insert it
				  if (elemT == NULL)  elemT = xmlDoc->createElementType($2);
				  elems->setNamedItem(elemT);

				  globElemT = elemT;
				}
			 	AttDef  DTD_GREATER
				{
				}
			;


AttDef			:       /* empty */
				{
				}

			|	AttDef  OneAttDef
				{
				}
			;


OneAttDef		:	att_name  
				{
				  // first look if att_name exist in the list of attributes
				  AttrType* attrT = (AttrType*) attribs->getNamedItem($1);

				  // if not, create and insert it
				  if (attrT == NULL)  attrT = xmlDoc->createAttrType($1);
				  attribs->setNamedItem(attrT);

				  // add it to the list of element
				  try { globElemT->setAttributeNode(attrT); }
				  catch (DOMException& e)
				      { cout << e << endl;  exit(1); }

				  attrT->setParentNode(globElemT);

				  globAttrT = attrT;
				}
				AttType  DefaultDecl
				{
				}
			;

 
att_name		:	DTD_NAME
				{
				  $$ = $1;
				}
			;


AttType			:	StringType
				{
				}

			|	TokenizedType
				{
				}

			|	EnumeratedType
				{
				}
			;


StringType	        :	DTD_CDATA
				{
				  globAttrT->setType(AttrType::CDATA);
				}
			;


TokenizedType		:	DTD_ID
				{
				  globAttrT->setType(AttrType::ID);
				}

	        /*	|	DTD_IDREF
			|	DTD_IDREFS
			|	DTD_ENTITY           to be done later!
			|	DTD_ENTITIES
			|	DTD_NMTOKEN_
		        |	DTD_NMTOKENS_  */
			;	

  
EnumeratedType		:	Enumeration
				{
				  globAttrT->setType(AttrType::ENUMERATION);
				}

	      /*	|	NotationType   */
			;


Enumeration		:	DTD_LPAR  nmtoken_list  list_el  DTD_RPAR
				{
				  globAttrT->insertEnumValue($3);
				}
			;


list_el			:	DTD_NAME 
				{
				  $$ = $1;
				}

	/*		|       DTD_NMTOKEN    */
			;


nmtoken_list            :	/* empty */
				{
				}

			|	nmtoken_list  list_el  DTD_PIPESTEM
				{
				  globAttrT->insertEnumValue($2);
				}
			;


DefaultDecl		:	DTD_REQUIRED
				{
				  globAttrT->setDefaultType(AttrType::REQUIRED);
				}

			|	DTD_IMPLIED
				{
				  globAttrT->setDefaultType(AttrType::IMPLIED);
				}

			|	DTD_FIXED   AttValue
				{
				  globAttrT->setDefaultType(AttrType::FIXED);
				  globAttrT->setValue($2);
				}

			|	AttValue
				{
				  globAttrT->setDefaultType(AttrType::DEFAULT);
				  globAttrT->setValue($1);
				}
			;


AttValue		:	DTD_QUOTE  DTD_NAME  DTD_QUOTE
				{
				  $$ = $2;
				}
			;	

%%

#include "lex.yy.c"

yyerror(char *s)
{
 fprintf(stderr, "%s", s);
}

int yyparse();

