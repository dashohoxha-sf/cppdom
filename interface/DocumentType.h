
#ifndef  _DocumentType_H_
#define  _DocumentType_H_

#include "DOM.h"

class DocumentType : Node {
public:
  String              getName();
  NamedNodeMap*       getEntities();
  NamedNodeMap*       getNotations();
};


#endif  // _DocumentType_H_

