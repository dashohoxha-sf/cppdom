
#ifndef  _CharacterData_H_
#define  _CharacterData_H_

#include "DOM.h"

class CharacterData : Node {

public:
  String             getData()
                                       throw (DOMException&);
  void               setData(String data)
                                       throw (DOMException&);
  int                getLength();
  String             substringData(int offset, int count)
                                       throw (DOMException&);
  void               appendData(String arg)
                                       throw (DOMException&);
  void               insertData(int offset, String arg)
                                       throw (DOMException&);
  void               deleteData(int offset, int count)
                                       throw (DOMException&);
  void               replaceData(int offset, int count, String arg)
                                       throw (DOMException&);
};


#endif  // _CharacterData_H_

