
#ifndef  _NamedNodeMap_H_
#define  _NamedNodeMap_H_

#include "DOM.h"

class NamedNodeMap {
public:
  Node*              getNamedItem(String name);
  Node*              setNamedItem(Node* arg)
                                         throw (DOMException&);
  Node*              removeNamedItem(String name)
                                         throw (DOMException&);
  Node*              item(int index);
  int                getLength();
};


#endif  // _NamedNodeMap_H_

