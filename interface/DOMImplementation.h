
#ifndef  _DOMImplementation_H_
#define  _DOMImplementation_H_

#include "DOM.h"

class DOMImplementation {
public:
  boolean            hasFeature(String feature, String version);
};


#endif  // _DOMImplementation_H_

