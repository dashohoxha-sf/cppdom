
#ifndef  _Attr_H_
#define  _Attr_H_

#include "DOM.h"

class Attr : Node {
public:
  String             getName();
  boolean            getSpecified();
  String             getValue();
  void               setValue(String value);
};


#endif  // _Attr_H_

