
#ifndef  _Node_H_
#define  _Node_H_

#include "DOM.h"


class Node {

public:
  static const unsigned short    ELEMENT_NODE                = 1;
  static const unsigned short    ATTRIBUTE_NODE              = 2;
  static const unsigned short    TEXT_NODE                   = 3;
  static const unsigned short    CDATA_SECTION_NODE          = 4;
  static const unsigned short    ENTITY_REFERENCE_NODE       = 5;
  static const unsigned short    ENTITY_NODE                 = 6;
  static const unsigned short    PROCESSING_INSTRUCTION_NODE = 7;
  static const unsigned short    COMMENT_NODE                = 8;
  static const unsigned short    DOCUMENT_NODE               = 9;
  static const unsigned short    DOCUMENT_TYPE_NODE          = 10;
  static const unsigned short    DOCUMENT_FRAGMENT_NODE      = 11;
  static const unsigned short    NOTATION_NODE               = 12;

  virtual String             getNodeName();
  virtual String             getNodeValue()
                                                 throw (DOMException&);
  virtual void               setNodeValue(String nodeValue)
                                                 throw (DOMException&);
  virtual short              getNodeType();
  virtual Node*              getParentNode();
  virtual NodeList*          getChildNodes();
  virtual Node*              getFirstChild();
  virtual Node*              getLastChild();
  virtual Node*              getPreviousSibling();
  virtual Node*              getNextSibling();
  virtual NamedNodeMap*      getAttributes();
  virtual Document*          getOwnerDocument();
  virtual Node*              insertBefore(Node* newChild, Node* refChild)
                                                 throw (DOMException&);
  virtual Node*              replaceChild(Node* newChild, Node* oldChild)
                                                 throw (DOMException&);
  virtual Node*              removeChild(Node* oldChild)
                                                 throw (DOMException&);
  virtual Node*              appendChild(Node* newChild)
                                                 throw (DOMException&);
  virtual boolean            hasChildNodes();
  virtual Node*              cloneNode(boolean deep);
  
};


#endif  // _Node_H_

