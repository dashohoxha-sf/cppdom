
#ifndef  _Text_H_
#define  _Text_H_

#include "DOM.h"

class Text : CharacterData {
public:
  Text*               splitText(int offset)
                                      throw (DOMException&);
};


#endif  // _Text_H_

