
#ifndef  _ProcessingInstruction_H_
#define  _ProcessingInstruction_H_

#include "DOM.h"

class ProcessingInstruction : Node {
public:
  String             getTarget();
  String             getData();
  void               setData(String data)
                                      throw (DOMException&);
};


#endif  // _ProcessingInstruction_H_

