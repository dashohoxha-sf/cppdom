
#ifndef  _Document_H_
#define  _Document_H_

#include "DOM.h"

class Document : Node {

public:
  DocumentType*       getDoctype();
  DOMImplementation*  getImplementation();
  Element*            getDocumentElement();
  Element*            createElement(String tagName)
                                               throw (DOMException&);
  DocumentFragment*   createDocumentFragment();
  Text*               createTextNode(String data);
  Comment*            createComment(String data);
  CDATASection*       createCDATASection(String data)
                                               throw (DOMException&);
  ProcessingInstruction*
                      createProcessingInstruction(String target, String data)
                                               throw (DOMException&);
  Attr*               createAttribute(String name)
                                               throw (DOMException&);
  EntityReference*    createEntityReference(String name)
                                               throw (DOMException&);
  NodeList*           getElementsByTagName(String tagname);
};


#endif  // _Document_H_

