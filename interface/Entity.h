
#ifndef  _Entity_H_
#define  _Entity_H_

#include "DOM.h"

class Entity : Node {
public:
  String             getPublicId();
  String             getSystemId();
  String             getNotationName();
};


#endif  // _Entity_H_

