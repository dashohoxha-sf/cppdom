
#ifndef  _Notation_H_
#define  _Notation_H_

#include "DOM.h"

class Notation : Node {
public:
  String             getPublicId();
  String             getSystemId();
};


#endif  // _Notation_H_

