/*

C++DOM, Implementation of DOM Core in C++.
Copyright (C) 1999 Dashamir Hoxha & Aurel Cami

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


E-mail:
   Aurel    Cami  : e103686@ceng.metu.edu.tr
   Dashamir Hoxha : e106708@ceng.metu.edu.tr

H-page:
   Aurel    Cami  : http://www.ceng.metu.edu.tr/~e103686/home_page
   Dashamir Hoxha : http://www.ceng.metu.edu.tr/~e106708
*/

  
#ifndef  _DOM_H_
#define  _DOM_H_

#define   String   char*
#define   boolean  short

class  Attr;
class  CDATASection;
class  CharacterData;
class  Comment;
class  DocumentFragment;
class  Document;
class  DocumentType;
class  DOMImplementation;
class  Element;
class  Entity;
class  EntityReference;
class  NamedNodeMap;
class  NodeList;
class  Node;
class  Notation;
class  ProcessingInstruction;
class  Text;
class  DOMException;

#include  <iostream.h>

#include  "DOMException.h"
#include  "Node.h"
#include  "Attr.h"
#include  "NodeList.h"
#include  "CharacterData.h"
#include  "Text.h"
#include  "CDATASection.h"
#include  "Comment.h"
#include  "DocumentFragment.h"
#include  "Document.h"
#include  "DocumentType.h"
#include  "DOMImplementation.h"
#include  "Element.h"
#include  "Entity.h"
#include  "EntityReference.h"
#include  "NamedNodeMap.h"
#include  "Notation.h"
#include  "ProcessingInstruction.h"


#endif  // _DOM_H_
