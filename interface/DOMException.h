
#ifndef  _DOMException_H_
#define  _DOMException_H_

#include "DOM.h"

class DOMException {

public:
  DOMException(short code, String message);
  friend ostream& operator<< (ostream&, DOMException&);

  // ExceptionCode
  static const short           INDEX_SIZE_ERR              = 1;
  static const short           DOMSTRING_SIZE_ERR          = 2;
  static const short           HIERARCHY_REQUEST_ERR       = 3;
  static const short           WRONG_DOCUMENT_ERR          = 4;
  static const short           INVALID_CHARACTER_ERR       = 5;
  static const short           NO_DATA_ALLOWED_ERR         = 6;
  static const short           NO_MODIFICATION_ALLOWED_ERR = 7;
  static const short           NOT_FOUND_ERR               = 8;
  static const short           NOT_SUPPORTED_ERR           = 9;
  static const short           INUSE_ATTRIBUTE_ERR         = 10;
};


#endif  // _DOMException_H_

