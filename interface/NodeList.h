
#ifndef  _NodeList_H_
#define  _NodeList_H_

#include "DOM.h"

class NodeList {
public:
  Node*     item(int index);
  int       getLength();
};


#endif  // _NodeList_H_

