
#ifndef  _DocumentFragment_H_
#define  _DocumentFragment_H_

#include "DOM.h"

class DocumentFragment : public Node {
public:
  DocumentFragment() : Node(DOCUMENT_FRAGMENT_NODE, "#document-fragment") {}
};


#endif  // _DocumentFragment_H_

