
#ifndef  _Text_H_
#define  _Text_H_

#include "DOM.h"

class Text : public CharacterData {
public:
  Text*               splitText(int offset)
                                      throw (DOMException&);
public:
  Text(String cData = "");
  ~Text() { ((CharacterData*)this)->~CharacterData(); }
  void   test();
};


#endif  // _Text_H_

