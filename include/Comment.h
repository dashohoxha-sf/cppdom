
#ifndef  _Comment_H_
#define  _Comment_H_

#include "DOM.h"

class Comment : public CharacterData {
public:
  Comment(String cData = "") : CharacterData(cData)
            { setNodeType(COMMENT_NODE);  setNodeName("#comment"); }
};


#endif  // _Comment_H_

