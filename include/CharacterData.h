
#ifndef  _CharacterData_H_
#define  _CharacterData_H_

#include "DOM.h"

class CharacterData : public Node {

public:
  String             getData()
                                       throw (DOMException&);
  void               setData(String data)
                                       throw (DOMException&);
  int                getLength();
  String             substringData(int offset, int count)
                                       throw (DOMException&);
  void               appendData(String arg)
                                       throw (DOMException&);
  void               insertData(int offset, String arg)
                                       throw (DOMException&);
  void               deleteData(int offset, int count)
                                       throw (DOMException&);
  void               replaceData(int offset, int count, String arg)
                                       throw (DOMException&);
private:
  String             charData;  // a null terminated string
public:
  CharacterData() { charData = NULL; }
  CharacterData(String cData);
  ~CharacterData()   { ((Node*)this)->~Node();  delete charData; }
  friend ostream&    operator<< (ostream&, CharacterData&);
  void               test();
};


#endif  // _CharacterData_H_

