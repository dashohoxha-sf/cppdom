
#ifndef  _ContentType_H_
#define  _ContentType_H_

#include "DOM.h"

class ContentType : public Node {
public:
  // multiplicity constants
  static const short        ONLY_ONE      = 1;
  static const short        ZERO_OR_ONE   = 2;
  static const short        ONE_OR_MORE   = 3;
  static const short        ZERO_OR_MORE  = 4;

  // ordering constants
  static const short        SEQUENCE      = 5;
  static const short        CHOICE        = 6;

  // type constants
  static const short        EMPTY         = 7;
  static const short        ANY           = 8;
  static const short        CHILDREN      = 9;
  static const short        MIXED         = 10;

  short                 getMultiplicity();
  short                 getOrdering();
  short                 getCType();

private:
  short                 multiplicity;
  short                 ordering;
  short                 cType;  // can be  EMPTY, ANY, CHILDREN or MIXED

public:
  ContentType(short mult = ONLY_ONE, short ord = SEQUENCE);
  void                  setMultiplicity(short mult);
  void                  setOrdering(short order);
  void                  setCType(short type);

  // for debug
  virtual void          print(ostream&);
  friend ostream&       operator<< (ostream&, ContentType&);
  void                  test();
};


inline short ContentType::getMultiplicity()  { return multiplicity; }

inline short ContentType::getOrdering()      { return ordering; }

inline short ContentType::getCType()         { return cType; }

inline void  ContentType::setMultiplicity(short mult)  { multiplicity = mult; }

inline void  ContentType::setOrdering(short order)     { ordering = order; }

inline void  ContentType::setCType(short type)         { cType = type; }


#endif  //  _ContentType_H_
