
#ifndef  _Attr_H_
#define  _Attr_H_

#include "DOM.h"

class Attr : public Node {
public:
  String             getName();
  boolean            getSpecified();
  String             getValue();
  void               setValue(String value);
private:
  boolean            specified;
public:
  Attr(String name, String value = NULL);
public:
  friend ostream&    operator<< (ostream&, Attr&);
  void               test();
};


#endif  // _Attr_H_

