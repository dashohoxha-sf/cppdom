
#ifndef  _Document_H_
#define  _Document_H_

#include "DOM.h"

class Document : public Node {

public:
  DocumentType*       getDoctype();
  DOMImplementation*  getImplementation();
  Element*            getDocumentElement();
  Element*            createElement(String tagName)
                                               throw (DOMException&);
  DocumentFragment*   createDocumentFragment();
  Text*               createTextNode(String data);
  Comment*            createComment(String data);
  CDATASection*       createCDATASection(String data)
                                               throw (DOMException&);
  ProcessingInstruction*
                      createProcessingInstruction(String target, String data)
                                               throw (DOMException&);
  Attr*               createAttribute(String name)
                                               throw (DOMException&);
  EntityReference*    createEntityReference(String name)
                                               throw (DOMException&);
  NodeList*           getElementsByTagName(String tagname);

  // for DTD
  ElementType*        createElementType(String tagName);
  AttrType*           createAttrType(String attrName);
  ContentType*        createContentType();
  PCDATA*             createPCDATA();

private:
  DocumentType*       docType;
  DOMImplementation*  implementation;
  Element*            rootElement;

public:
  Document(String docName);
  ~Document();
  DocumentType*       setDoctype(DocumentType* docT);
  DOMImplementation*  setImplementation(DOMImplementation* impl);
  Element*            setDocumentElement(Element* elem);

  void                test();
};


#endif  // _Document_H_

