
#ifndef  _DocumentType_H_
#define  _DocumentType_H_

#include "DOM.h"

class DocumentType : public Node {

public:
  String              getName();
  NamedNodeMap*       getEntities();
  NamedNodeMap*       getNotations();
  NamedNodeMap*       getElements();
  // NamedNodeMap*       getAttributes();  // inherited by Node

private:
  NamedNodeMap*       entities;
  NamedNodeMap*       notations;
  NamedNodeMap*       elements;
  // attributes is inherited by Node

public:
  DocumentType(String name);
  ~DocumentType();

  Entity*             addEntity(Entity* entity);
  Notation*           addNotation(Notation* notation);
  ElementType*        addElementType(ElementType* elemType);
  AttrType*           addAttrType(AttrType* attrType);
  boolean             checkDTD();

public:
  virtual void        print(ostream&);
  friend ostream&     operator<< (ostream&, DocumentType&);
  void                test();
};


#endif  // _DocumentType_H_

