
#ifndef  _AttrType_H_
#define  _AttrType_H_

#include "DOM.h"

class AttrType : public Attr {
public:
  // type constants
  static const short        CDATA         = 1;
  static const short        ENUMERATION   = 2;
  static const short        ID            = 3;
  // static const short        ...           = 4;

  // default type constants
  static const short        REQUIRED      = 11;
  static const short        IMPLIED       = 12;
  static const short        FIXED         = 13;
  static const short        DEFAULT       = 14;
  

  short                getType();
  short                getDefaultType();
  void                 insertEnumValue(String enumVal)
                                               throw (DOMException&);
  boolean              checkAttrValue(String value);

private:
  short                attrType;
  short                defaultType;
  boolean              isInEnumList(String value);

public:
  AttrType(String attrName, short type = CDATA);
  void                 setType(short type);
  void                 setDefaultType(short defType);
  void                 setElement(ElementType* elem);
  void                 printEnum(ostream&);

  // for debuging
  virtual void         print(ostream&);
  friend ostream&      operator<< (ostream&, AttrType&);
  void                 test();
};


inline short AttrType::getType()            { return attrType; }

inline short AttrType::getDefaultType()     { return defaultType; }

inline void  AttrType::setType(short type)  { attrType = type; }

inline void  AttrType::setDefaultType(short defType)  { defaultType = defType; }


#endif  // _AttrType_H_

