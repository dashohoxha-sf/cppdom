
#ifndef  _DOMImplementation_H_
#define  _DOMImplementation_H_

#include "DOM.h"

class DOMImplementation {
public:
  boolean            hasFeature(String feature, String version);

  boolean            docParse(String xmlFile, String dtdFile);
  DocumentFragment*  xmlParse(String fileName);
  DocumentType*      dtdParse(String fileName);

public:
  Node*              getCurrentNode() { return currentNode; }
  Node*              setCurrentNode(Node* newNode);  // returns the old one
  Document*          getOwnerDoc() { return ownerDoc; }

  DOMImplementation(Document* owner) { ownerDoc = owner; }

private:
  Document*          ownerDoc;
  Node*              currentNode;

public:
  int                depth;
};


#endif  // _DOMImplementation_H_

