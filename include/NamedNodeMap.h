
#ifndef  _NamedNodeMap_H_
#define  _NamedNodeMap_H_

#include "DOM.h"

class NamedNodeMap {

public:
  Node*              getNamedItem(String name);
  Node*              setNamedItem(Node* arg)
                                         throw (DOMException&);
  Node*              removeNamedItem(String name)
                                         throw (DOMException&);
  Node*              item(int index);
  int                getLength();

private:
  Node**             array;
  int                length;      // number of nodes in array
  int                size;        // size of array
  static const       STEP = 5;    // step of increasing array size
  void               enlarge_array();   // increase the size

public:
  NamedNodeMap*      clone();   // only itself and the nodes it contains
        NamedNodeMap();
        ~NamedNodeMap();
  friend ostream&    operator<< (ostream&, NamedNodeMap&);
  void               test();
};


#endif  // _NamedNodeMap_H_

