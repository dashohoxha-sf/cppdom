
#ifndef  _Notation_H_
#define  _Notation_H_

#include "DOM.h"

class Notation : public Node {
public:
  String             getPublicId();
  String             getSystemId();
private:
  String             publicId;
  String             systemId;
public:
  Notation(String name, String pubId, String sysId);
  ~Notation();
  friend ostream&    operator<< (ostream&, Notation&);
  void               test();
};


#endif  // _Notation_H_

