
#ifndef  _NodeList_H_
#define  _NodeList_H_

#include "DOM.h"

class NodeList {
public:
  Node*             item(int index);
  int               getLength();

private:
  Node**            array;
  int               length;     // number of nodes in array
  int               size;       // size of array
  static const      STEP = 5;   // step by which array size is increased
  void              enlarge_array();   // increase the size

public:
  NodeList();
  ~NodeList();
  int               indexOf(Node* node); // -1 if not found
  void              append(Node* node);
  Node*             removeAt(int idx);   // return the node removed
  Node*             setAt(Node* node, int idx);
  Node*             insertAt(Node* node, int idx);
  NodeList*         clone();  // makes a deep copy of itself and its nodes

public:
  friend ostream&   operator<< (ostream&, NodeList&);
                    // prints the names of its nodes (in order)
  void              test();            // self testing
};


#endif  // _NodeList_H_

