
#ifndef  _PCDATA_H_
#define  _PCDATA_H_

#include "DOM.h"

class PCDATA : public Node {
public:
  PCDATA() : Node(PCDATA_NODE, "#PCDATA")  { }
};


#endif  // _PCDATA_H_

