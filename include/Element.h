
#ifndef  _Element_H_
#define  _Element_H_

#include "DOM.h"

class Element : public Node {

public:
  String             getTagName();
  String             getAttribute(String name);
  void               setAttribute(String name, String value)
                                         throw (DOMException&);
  void               removeAttribute(String name)
                                         throw (DOMException&);
  Attr*              getAttributeNode(String name);
  Attr*              setAttributeNode(Attr* newAttr)
                                         throw (DOMException&);
  Attr*              removeAttributeNode(Attr* oldAttr)
                                         throw (DOMException&);
  NodeList*          getElementsByTagName(String name);
  void               normalize();

public:
  Element(String tagName) : Node(ELEMENT_NODE, tagName) {}
  ~Element();
public:
  friend ostream&    operator<< (ostream&, Element&);
  void               test();
};


#endif  // _Element_H_

