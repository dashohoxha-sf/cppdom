
#ifndef  _ElementType_H_
#define  _ElementType_H_

#include "DOM.h"

class ElementType : public Element {

public:
  // multiplicity constants
  static const short        ONLY_ONE      = 1;
  static const short        ZERO_OR_ONE   = 2;
  static const short        ONE_OR_MORE   = 3;
  static const short        ZERO_OR_MORE  = 4;

  short                 getMultiplicity();
  ContentType*          getContentType();

private:
  short                 multiplicity;
  ContentType*          contType;

public:
  ElementType(String tagName, short mult = ONLY_ONE);

  void                  setMultiplicity(short mult);
  void                  setContentType(ContentType* ct);

  // for debuging
  virtual void          print(ostream&);
  friend ostream&       operator<< (ostream&, ElementType&);
  void                  test();
  
};


inline short ElementType::getMultiplicity()            { return multiplicity; }

inline ContentType* ElementType::getContentType()      { return contType; }

inline void  ElementType::setMultiplicity(short mult)  { multiplicity = mult; }

inline void  ElementType::setContentType(ContentType* ct)  { contType = ct; }


#endif  // _ElementType_H_
