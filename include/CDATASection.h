
#ifndef  _CDATASection_H_
#define  _CDATASection_H_

#include "DOM.h"

class CDATASection : public Text {
public:
  CDATASection(String cData = "") : Text(cData)
        { setNodeType(CDATA_SECTION_NODE); setNodeName("#cdata-section"); }
};


#endif  // _CDATASection_H_

